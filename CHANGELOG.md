# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.4] - 2022-07-17
 - Update servant version

## [2.0.3] - 2021-09-24
 - Update dependencies due to security check

## [2.0.2] - 2021-06-10
## [2.0.1] - 2021-06-10
### Changed
- Update data API, use left and right property instead of data

## [1.0.4] - 2021-06-09
### Changed
 - Export types for compare results

## [1.0.3] - 2020-11-02
## [1.0.2] - 2020-02-02
## [1.0.1] - 2020-01-03
## [1.0.0] - 2020-01-03
### Added
 - Implementation of text diff algorithm based on 'An O(NP) Sequence Comparison Algorithm'
 - Support for compare text abd arrays of objects with `.toString()` method

[Unreleased]: https://gitlab.com/stanislavhacker/onp/compare/2.0.4...master
[2.0.4]: https://gitlab.com/stanislavhacker/onp/compare/2.0.3...2.0.4
[2.0.3]: https://gitlab.com/stanislavhacker/onp/compare/2.0.2...2.0.3
[2.0.2]: https://gitlab.com/stanislavhacker/onp/compare/2.0.1...2.0.2
[2.0.1]: https://gitlab.com/stanislavhacker/onp/compare/1.0.4...2.0.1
[1.0.4]: https://gitlab.com/stanislavhacker/onp/compare/1.0.3...1.0.4
[1.0.3]: https://gitlab.com/stanislavhacker/onp/compare/1.0.2...1.0.3
[1.0.2]: https://gitlab.com/stanislavhacker/onp/compare/1.0.1...1.0.2
[1.0.1]: https://gitlab.com/stanislavhacker/onp/compare/1.0.0...1.0.1
[1.0.0]: https://gitlab.com/stanislavhacker/onp/commit/8a1f7abad78d149d972fb79d82f93afe2a866b97
